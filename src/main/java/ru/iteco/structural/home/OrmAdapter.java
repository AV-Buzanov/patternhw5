package ru.iteco.structural.home;

import ru.iteco.structural.home.adapters.first.Target;
import ru.iteco.structural.home.orm.first.IFirstOrm;
import ru.iteco.structural.home.orm.second.ISecondOrm;
import ru.iteco.structural.home.orm.second.ISecondOrmContext;

public class OrmAdapter implements Target {

    private IFirstOrm<DbUserEntity> userEntityFirstOrm;

    private IFirstOrm<DbUserInfoEntity> userInfoFirstOrm;

    private ISecondOrmContext secondOrmContext;

    public OrmAdapter(IFirstOrm<DbUserEntity> userEntityFirstOrm,
                      IFirstOrm<DbUserInfoEntity> userInfoFirstOrm) {
        this.userEntityFirstOrm = userEntityFirstOrm;
        this.userInfoFirstOrm = userInfoFirstOrm;
    }

    public OrmAdapter(ISecondOrm secondOrm) {
        if (secondOrm != null) {
            this.secondOrmContext = secondOrm.getContext();
        }
    }

    @Override
    public void createUser(DbUserEntity entity) {
        if (secondOrmContext != null) {
            secondOrmContext.getUsers().add(entity);
        } else if (userEntityFirstOrm != null) {
            userEntityFirstOrm.create(entity);
        }
    }

    @Override
    public DbUserEntity readUser(Long id) {
        if (secondOrmContext != null) {
            return secondOrmContext.getUsers()
                    .stream()
                    .filter(s -> s.getId().equals(id))
                    .findAny()
                    .orElse(null);
        } else if (userEntityFirstOrm != null) {
            return userEntityFirstOrm.read(Math.toIntExact(id));
        }
        return null;
    }

    @Override
    public void updateUser(DbUserEntity entity) {
        if (secondOrmContext != null) {
            DbUserEntity dbUserEntity = readUser(entity.getId());
            if (dbUserEntity != null) {
                secondOrmContext.getUsers().remove(dbUserEntity);
                secondOrmContext.getUsers().add(entity);
            }
        } else if (userEntityFirstOrm != null) {
            userEntityFirstOrm.update(entity);
        }
    }

    @Override
    public void deleteUser(DbUserEntity entity) {
        if (secondOrmContext != null) {
            secondOrmContext.getUsers().remove(entity);
        } else if (userEntityFirstOrm != null) {
            userEntityFirstOrm.delete(entity);
        }
    }

    @Override
    public void createUserInfo(DbUserInfoEntity entity) {
        if (secondOrmContext != null) {
            secondOrmContext.getUserInfos().add(entity);
        } else if (userInfoFirstOrm != null) {
            userInfoFirstOrm.create(entity);
        }
    }

    @Override
    public DbUserInfoEntity readUserInfo(Long id) {
        if (secondOrmContext != null) {
            return secondOrmContext.getUserInfos()
                    .stream()
                    .filter(s -> s.getId().equals(id))
                    .findAny()
                    .orElse(null);
        } else if (userInfoFirstOrm != null) {
            return userInfoFirstOrm.read(Math.toIntExact(id));
        }
        return null;
    }

    @Override
    public void updateUserInfo(DbUserInfoEntity entity) {
        if (secondOrmContext != null) {
            DbUserInfoEntity dbUserEntity = readUserInfo(entity.getId());
            if (dbUserEntity != null) {
                secondOrmContext.getUserInfos().remove(dbUserEntity);
                secondOrmContext.getUserInfos().add(entity);
            }
        } else if (userInfoFirstOrm != null) {
            userInfoFirstOrm.update(entity);
        }
    }

    @Override
    public void deleteUserInfo(DbUserInfoEntity entity) {
        if (secondOrmContext != null) {
            secondOrmContext.getUserInfos().remove(entity);
        } else if (userInfoFirstOrm != null) {
            userInfoFirstOrm.delete(entity);
        }
    }
}
