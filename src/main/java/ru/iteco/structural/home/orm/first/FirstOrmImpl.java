package ru.iteco.structural.home.orm.first;

import ru.iteco.structural.home.IDbEntity;

import java.util.HashMap;
import java.util.Map;

public class FirstOrmImpl<T extends IDbEntity> implements IFirstOrm<T> {
    Map<Long, T> db = new HashMap<>();

    @Override
    public void create(T entity) {
        db.put(entity.getId(), entity);
    }

    @Override
    public T read(int id) {
        return db.get((long) id);
    }

    @Override
    public void update(T entity) {
        db.replace(entity.getId(), entity);
    }

    @Override
    public void delete(T entity) {
        db.remove(entity.getId());
    }
}
