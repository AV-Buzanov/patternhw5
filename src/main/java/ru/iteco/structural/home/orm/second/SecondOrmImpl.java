package ru.iteco.structural.home.orm.second;

import ru.iteco.structural.home.DbUserEntity;
import ru.iteco.structural.home.DbUserInfoEntity;

import java.util.HashSet;
import java.util.Set;

public class SecondOrmImpl implements ISecondOrm {

    private final ISecondOrmContext context = new ISecondOrmContext() {
        private final Set<DbUserEntity> entities = new HashSet<>();
        private final Set<DbUserInfoEntity> infoEntitySet = new HashSet<>();

        @Override
        public Set<DbUserEntity> getUsers() {
            return entities;
        }

        @Override
        public Set<DbUserInfoEntity> getUserInfos() {
            return infoEntitySet;
        }
    };

    @Override
    public ISecondOrmContext getContext() {
        return context;
    }
}
