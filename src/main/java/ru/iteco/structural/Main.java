package ru.iteco.structural;

import ru.iteco.structural.home.DbUserEntity;
import ru.iteco.structural.home.DbUserInfoEntity;
import ru.iteco.structural.home.OrmAdapter;
import ru.iteco.structural.home.adapters.first.Target;
import ru.iteco.structural.home.orm.first.FirstOrmImpl;
import ru.iteco.structural.home.orm.first.IFirstOrm;
import ru.iteco.structural.home.orm.second.ISecondOrm;
import ru.iteco.structural.home.orm.second.SecondOrmImpl;

import java.time.LocalDateTime;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {

    public static void main(String[] args) {
        IFirstOrm<DbUserEntity> entityFirstOrm = new FirstOrmImpl<>();
        IFirstOrm<DbUserInfoEntity> entityInfoFirstOrm = new FirstOrmImpl<>();
        ISecondOrm secondOrm = new SecondOrmImpl();
        Target adapter = new OrmAdapter(entityFirstOrm, entityInfoFirstOrm);
        addUser(adapter);
        adapter = new OrmAdapter(secondOrm);
        addUser(adapter);
        System.out.println(entityFirstOrm.read(2));
        System.out.println(entityInfoFirstOrm.read(1));
        System.out.println(secondOrm.getContext().getUsers());
        System.out.println(secondOrm.getContext().getUserInfos());
    }

    private static void addUser(Target adapter) {
        DbUserInfoEntity entityInfo = new DbUserInfoEntity(1L,"name", LocalDateTime.now());
        DbUserEntity entity = new DbUserEntity(2L, "login", "pass", entityInfo.getId());
        adapter.createUser(entity);
        adapter.createUserInfo(entityInfo);
    }
}
